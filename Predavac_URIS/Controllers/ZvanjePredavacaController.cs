﻿using Predavac_URIS.AuthFilters;
using Predavac_URIS.DataAccess;
using Predavac_URIS.Filters;
using Predavac_URIS.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Predavac_URIS.Controllers
{
    [RoutePrefix("api-predavac/zvanje_predavaca")]
    public class ZvanjePredavacaController : ApiController
    {
        // GET: api-predavac/zvanje_predavaca
        [Route("")]
        [HttpGet]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        public HttpResponseMessage GetZvanjePredavaca()
        {
            var zvanjePredavacaDB = new ZvanjePredavacaDB();
            return Request.CreateResponse(HttpStatusCode.OK, zvanjePredavacaDB.GetZvanjePredavaca());
        }

        // GET: api-predavac/zvanje_predavaca/5
        [HttpGet, Route("{id}")]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        public HttpResponseMessage GetZvanjePredavacaByID(int id)
        {
            var zvanjePredavacaDB = new ZvanjePredavacaDB();
            return Request.CreateResponse(HttpStatusCode.OK, zvanjePredavacaDB.GetZvanjePredavacaByID(id));
        }

        // POST: api-predavac/zvanje_predavaca
        [HttpPost, Route("")]
        [ValidateModelState(BodyRequired = true)]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage CreateZvanjePredavaca([FromBody]ZvanjePredavaca zvanjePredavaca)
        {
            var zvanjePredavacaDB = new ZvanjePredavacaDB();
            var created = zvanjePredavacaDB.CreateZvanjePredavaca(zvanjePredavaca);
            var response = Request.CreateResponse(HttpStatusCode.Created, created);
            response.Headers.Location = new Uri(Request.RequestUri + "/" + created.ZvanjePredavacaID);
            return response;
        }

        // PUT: api-predavac/zvanje_predavaca
        [HttpPut, Route("")]
        [ValidateModelState(BodyRequired = true)]
        [Authorize(Roles = RolesConst.ROLE_Admin_Predavac)]
        public HttpResponseMessage UpdateZvanjePredavaca([FromBody]ZvanjePredavaca zvanjePredavaca)
        {
            var zvanjePredavacaDB = new ZvanjePredavacaDB();
            zvanjePredavacaDB.UpdateZvanjePredavaca(zvanjePredavaca);
            return Request.CreateResponse(HttpStatusCode.OK, "Updated Successfully");
        }

        // DELETE: api-predavac/zvanje_predavaca
        [HttpDelete, Route("{id}")]
        [Authorize(Roles = RolesConst.ROLE_Admin_Predavac)]
        public HttpResponseMessage DeleteZvanjePredavaca(int id)
        {
            var zvanjePredavacaDB = new ZvanjePredavacaDB();
            zvanjePredavacaDB.DeleteZvanjePredavaca(id);
            return Request.CreateResponse(HttpStatusCode.OK, "Deleted Successfully");
        }
    }
}