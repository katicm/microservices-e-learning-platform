﻿using Predavac_URIS.AuthFilters;
using Predavac_URIS.DataAccess;
using Predavac_URIS.Filters;
using Predavac_URIS.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Predavac_URIS.Controllers
{
    [RoutePrefix("api-predavac/predavac")]
    public class PredavacController : ApiController
    {
        // GET: api-predavac/predavac
        [Route("")]
        [HttpGet]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        public HttpResponseMessage GetPredavac()
        {
            var predavacDB = new PredavacDB();
            return Request.CreateResponse(HttpStatusCode.OK, predavacDB.GetAllPredavac());
        }

        // GET: api-predavac/predavac/5
        [HttpGet, Route("{id}")]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        public async Task<HttpResponseMessage> GetPredavacByIDAsync(int id)
        {
            var predavacDB = new PredavacDB();
            PredavacWithVO predavac = predavacDB.GetPredavacByID(id);
            var predavacHttp = new PredavacHTTP();
            var authorization = Request.Headers.GetValues("Authorization").FirstOrDefault();
            predavac.Korisnik = await predavacHttp.GetKorisnikVOAsync(predavac.KorisnikID, authorization);
            predavac.Departman = await predavacHttp.GetDepartmanVOAsync(predavac.DepartmanID, authorization);
            return Request.CreateResponse(HttpStatusCode.OK, predavac);
        }

        // GET: api-predavac/predavac/departman/5
        [HttpGet, Route("departman/{id}")]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        public HttpResponseMessage GetByDepartmanID(int id)
        {
            var predavacDB = new PredavacDB();
            return Request.CreateResponse(HttpStatusCode.OK, predavacDB.GetPredavacByDepartmanID(id));
        }

        // POST: api-predavac/predavac
        [HttpPost, Route("")]
        [ValidateModelState(BodyRequired = true)]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage CreatePredavac([FromBody]Predavac predavac)
        {
            var predavacDB = new PredavacDB();
            var created = predavacDB.CreatePredavac(predavac);
            var response = Request.CreateResponse(HttpStatusCode.Created, created);
            response.Headers.Location = new Uri(Request.RequestUri + "/" + created.PredavacID);
            return response;
        }

        // PUT: api-predavac/predavac
        [HttpPut, Route("")]
        [ValidateModelState(BodyRequired = true)]
        [Authorize(Roles = RolesConst.ROLE_Admin_Predavac)]
        public HttpResponseMessage UpdatePredavac([FromBody]Predavac predavac)
        {
            var predavacDB = new PredavacDB();
            predavacDB.UpdatePredavac(predavac);
            return Request.CreateResponse(HttpStatusCode.OK, "Updated Successfully");
        }

        // DELETE: api-predavac/predavac/5
        [HttpDelete, Route("{id}")]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage DeletePredavac(int id)
        {
            var predavacDB = new PredavacDB();
            predavacDB.DeleteKorisnik(id);
            return Request.CreateResponse(HttpStatusCode.OK, "Deleted Successfully");
        }
    }
}