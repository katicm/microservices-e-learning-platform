﻿using Predavac_URIS.AuthFilters;
using Predavac_URIS.Filters;
using Predavac_URIS.Handlers;
using System.Web.Http;

namespace Predavac_URIS
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            config.SuppressHostPrincipal();

            //messages
            config.MessageHandlers.Add(new PipelineTimerHandler());

            //auth
            config.Filters.Add(new BasicAuthFilterAttribute());
            config.Filters.Add(new TokenAuthFilter());

            config.Filters.Add(new AuthorizeAttribute());

            config.Filters.Add(new AccessDataExceptionFilter());

            // Web API routes
            config.MapHttpAttributeRoutes();
        }
    }
}
