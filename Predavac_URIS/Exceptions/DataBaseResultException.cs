﻿using System;

namespace Predavac_URIS.Exceptions
{
    public class DataBaseResultException : Exception
    {
        public DataBaseResultException()
        {

        }

        public DataBaseResultException(string message) : base(message)
        {

        }

        public DataBaseResultException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}