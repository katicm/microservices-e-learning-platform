﻿using System.ComponentModel.DataAnnotations;

namespace Predavac_URIS.Models
{
    public class ZvanjePredavaca
    {
        public int ZvanjePredavacaID { get; set; }

        [Required]
        [MaxLength(100)]
        public string NazivZvanjaPredavaca { get; set; }
    }
}