﻿using System.Configuration;

namespace Predavac_URIS.DataAccess
{
    public class DBConnectionString
    {
        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString; }
        }
    }
}