﻿using Predavac_URIS.Exceptions;
using Predavac_URIS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Predavac_URIS.DataAccess
{
    public class ZvanjePredavacaDB
    {
        public List<ZvanjePredavaca> GetZvanjePredavaca()
        {
            try
            {
                List<ZvanjePredavaca> result = new List<ZvanjePredavaca>();
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Predavac.ZvanjePredavaca", connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (SqlDataReader dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var zvanjePredavaca = new ZvanjePredavaca
                            {
                                ZvanjePredavacaID = Convert.ToInt32(dataReader["ZvanjePredavacaID"]),
                                NazivZvanjaPredavaca = Convert.ToString(dataReader["NazivZvanjaPredavaca"])
                            };

                            result.Add(zvanjePredavaca);
                        }
                    }
                }
                return result;
            }
            catch
            {
                throw new DataBaseResultException("Bad Request - Error Retrieving Data");
            }
        }

        public ZvanjePredavaca GetZvanjePredavacaByID(int id)
        {
            try
            {
                ZvanjePredavaca zvanjePredavaca = null;
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Predavac.ZvanjePredavaca where ZvanjePredavacaID=" + id, connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (SqlDataReader dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            zvanjePredavaca = new ZvanjePredavaca
                            {
                                ZvanjePredavacaID = Convert.ToInt32(dataReader["ZvanjePredavacaID"]),
                                NazivZvanjaPredavaca = Convert.ToString(dataReader["NazivZvanjaPredavaca"])
                            };
                        }
                    }
                    if (zvanjePredavaca == null)
                        throw new DataBaseResultException("Not Found - Error Retrieving Data", new IndexOutOfRangeException());
                }
                return zvanjePredavaca;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ZvanjePredavaca CreateZvanjePredavaca(ZvanjePredavaca zvanjePredavaca)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("INSERT INTO Predavac.ZvanjePredavaca VALUES(@NazivZvanjaPredavaca)", connection);
                    sqlCmd.Parameters.AddWithValue("NazivZvanjaPredavaca", zvanjePredavaca.NazivZvanjaPredavaca);
                    connection.Open();
                    sqlCmd.ExecuteNonQuery();
                    SqlCommand sqlZvanjePredavaca = new SqlCommand("SELECT TOP 1 * FROM Predavac.ZvanjePredavaca ORDER BY ZvanjePredavacaID DESC", connection);
                    ZvanjePredavaca last = new ZvanjePredavaca();
                    using (SqlDataReader zvanjePredavacaRead = sqlZvanjePredavaca.ExecuteReader())
                    {
                        while (zvanjePredavacaRead.Read())
                        {
                            last.ZvanjePredavacaID = Convert.ToInt32(zvanjePredavacaRead["ZvanjePredavacaID"]);
                            last.NazivZvanjaPredavaca = Convert.ToString(zvanjePredavacaRead["NazivZvanjaPredavaca"]);
                        }
                    }
                    return last;
                }
            }
            catch (Exception)
            {
                throw new DataBaseResultException("Unsuccessful Creating Data");
            }
        }

        public void UpdateZvanjePredavaca(ZvanjePredavaca zvanjePredavaca)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("UPDATE Predavac.ZvanjePredavaca SET NazivZvanjaPredavaca=@NazivZvanjaPredavaca WHERE ZvanjePredavacaID=@id"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("NazivZvanjaPredavaca", zvanjePredavaca.NazivZvanjaPredavaca);
                        sqlCmd.Parameters.AddWithValue("id", zvanjePredavaca.ZvanjePredavacaID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Updating Data");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteZvanjePredavaca(int id)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("DELETE FROM Predavac.ZvanjePredavaca where ZvanjePredavacaID=@id"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Deleting Data");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}