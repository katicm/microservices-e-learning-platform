﻿using Predavac_URIS.Models;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Predavac_URIS.DataAccess
{
    public class PredavacHTTP
    {
        public async Task<KorisnikInfoVO> GetKorisnikVOAsync(int korisnikID, string authorization)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:63288/");
                client.DefaultRequestHeaders.Accept.Clear();
                var token = authorization.Split(null)[1];
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api-korisnik/korisnik/" + korisnikID);
                var korisnik = new KorisnikInfoVO();
                if (response.IsSuccessStatusCode)
                {
                    korisnik = await response.Content.ReadAsAsync<KorisnikInfoVO>();
                }
                return korisnik;
            }
        }

        public async Task<DepartmanVO> GetDepartmanVOAsync(int departmanID, string authorization)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:63298/");
                client.DefaultRequestHeaders.Accept.Clear();
                var token = authorization.Split(null)[1];
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api-departman/departman/" + departmanID);
                var departman = new DepartmanVO();
                if (response.IsSuccessStatusCode)
                {
                    departman = await response.Content.ReadAsAsync<DepartmanVO>();
                }
                return departman;
            }
        }
    }
}