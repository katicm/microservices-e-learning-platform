﻿using Predavac_URIS.Exceptions;
using Predavac_URIS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Predavac_URIS.DataAccess
{
    public class PredavacDB
    {
        public List<Predavac> GetAllPredavac()
        {
            try
            {
                List<Predavac> result = new List<Predavac>();
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Predavac.Predavac", connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (SqlDataReader dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var predavac = new Predavac
                            {
                                PredavacID = Convert.ToInt32(dataReader["PredavacID"]),
                                DatumRodjenjaPredavaca = Convert.ToDateTime(dataReader["DatumRodjenjaPredavaca"]),
                                MestoRodjenjaPredavaca = Convert.ToString(dataReader["MestoRodjenjaPredavaca"]),
                                KatedraPredavaca = Convert.ToString(dataReader["KatedraPredavaca"]),
                                ZvanjePredavacaID = Convert.ToInt32(dataReader["ZvanjePredavacaID"]),
                                KorisnikID = Convert.ToInt32(dataReader["KorisnikID"]),
                                DepartmanID = Convert.ToInt32(dataReader["DepartmanID"])
                            };
                            result.Add(predavac);
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new DataBaseResultException("Bad Request - Error Retrieving Data");
            }
        }

        public PredavacWithVO GetPredavacByID(int id)
        {
            try
            {
                PredavacWithVO predavac = null;
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Predavac.Predavac where PredavacID=" + id, connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (SqlDataReader dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            predavac = new PredavacWithVO
                            {
                                PredavacID = Convert.ToInt32(dataReader["PredavacID"]),
                                DatumRodjenjaPredavaca = Convert.ToDateTime(dataReader["DatumRodjenjaPredavaca"]),
                                MestoRodjenjaPredavaca = Convert.ToString(dataReader["MestoRodjenjaPredavaca"]),
                                KatedraPredavaca = Convert.ToString(dataReader["KatedraPredavaca"]),
                                ZvanjePredavacaID = Convert.ToInt32(dataReader["ZvanjePredavacaID"]),
                                KorisnikID = Convert.ToInt32(dataReader["KorisnikID"]),
                                DepartmanID = Convert.ToInt32(dataReader["DepartmanID"])
                            };
                        }
                    }
                    if (predavac == null)
                        throw new DataBaseResultException("Not Found - Error Retrieving Data", new IndexOutOfRangeException());
                }
                return predavac;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Predavac> GetPredavacByDepartmanID(int departmanID)
        {
            try
            {
                List<Predavac> result = new List<Predavac>();
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Predavac.Predavac where DepartmanID=" + departmanID, connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (SqlDataReader dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var predavac = new Predavac
                            {
                                PredavacID = Convert.ToInt32(dataReader["PredavacID"]),
                                DatumRodjenjaPredavaca = Convert.ToDateTime(dataReader["DatumRodjenjaPredavaca"]),
                                MestoRodjenjaPredavaca = Convert.ToString(dataReader["MestoRodjenjaPredavaca"]),
                                KatedraPredavaca = Convert.ToString(dataReader["KatedraPredavaca"]),
                                ZvanjePredavacaID = Convert.ToInt32(dataReader["ZvanjePredavacaID"]),
                                KorisnikID = Convert.ToInt32(dataReader["KorisnikID"]),
                                DepartmanID = Convert.ToInt32(dataReader["DepartmanID"])
                            };

                            result.Add(predavac);
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new DataBaseResultException("Not Found - Error Retrieving Data", new IndexOutOfRangeException());
            }
        }

        public Predavac CreatePredavac(Predavac predavac)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("INSERT INTO Predavac.Predavac VALUES(@DatumRodjenjaPredavaca, @KatedraPredavaca, @MestoRodjenjaPredavaca, @ZvanjePredavacaID, @KorisnikID, @DepartmanID)", connection);
                    sqlCmd.Parameters.AddWithValue("DatumRodjenjaPredavaca", predavac.DatumRodjenjaPredavaca);
                    sqlCmd.Parameters.AddWithValue("KatedraPredavaca", predavac.KatedraPredavaca);
                    sqlCmd.Parameters.AddWithValue("MestoRodjenjaPredavaca", predavac.MestoRodjenjaPredavaca);
                    sqlCmd.Parameters.AddWithValue("ZvanjePredavacaID", predavac.ZvanjePredavacaID);
                    sqlCmd.Parameters.AddWithValue("KorisnikID", predavac.KorisnikID);
                    sqlCmd.Parameters.AddWithValue("DepartmanID", predavac.DepartmanID);

                    connection.Open();
                    sqlCmd.ExecuteNonQuery();
                    SqlCommand sqlUniverzitet = new SqlCommand("SELECT TOP 1 * FROM Predavac.Predavac ORDER BY PredavacID DESC", connection);
                    Predavac last = new Predavac();
                    using (SqlDataReader predavacRead = sqlUniverzitet.ExecuteReader())
                    {
                        while (predavacRead.Read())
                        {
                            last.PredavacID = Convert.ToInt32(predavacRead["PredavacID"]);
                            last.DatumRodjenjaPredavaca = Convert.ToDateTime(predavacRead["DatumRodjenjaPredavaca"]);
                            last.MestoRodjenjaPredavaca = Convert.ToString(predavacRead["MestoRodjenjaPredavaca"]);
                            last.KatedraPredavaca = Convert.ToString(predavacRead["KatedraPredavaca"]);
                            last.ZvanjePredavacaID = Convert.ToInt32(predavacRead["ZvanjePredavacaID"]);
                            last.KorisnikID = Convert.ToInt32(predavacRead["KorisnikID"]);
                            last.DepartmanID = Convert.ToInt32(predavacRead["DepartmanID"]);
                        }
                    }
                    return last;
                }
            }
            catch (Exception)
            {
                throw new DataBaseResultException("Unsuccessful Creating Data");
            }
        }

        public void UpdatePredavac(Predavac predavac)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("UPDATE Predavac.Predavac SET DatumRodjenjaPredavaca=@DatumRodjenjaPredavaca, KatedraPredavaca=@KatedraPredavaca, MestoRodjenjaPredavaca=@MestoRodjenjaPredavaca, ZvanjePredavacaID=@ZvanjePredavacaID, KorisnikID=@KorisnikID, DepartmanID=@DepartmanID WHERE PredavacID=@id"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("DatumRodjenjaPredavaca", predavac.DatumRodjenjaPredavaca);
                        sqlCmd.Parameters.AddWithValue("KatedraPredavaca", predavac.KatedraPredavaca);
                        sqlCmd.Parameters.AddWithValue("MestoRodjenjaPredavaca", predavac.MestoRodjenjaPredavaca);
                        sqlCmd.Parameters.AddWithValue("ZvanjePredavacaID", predavac.ZvanjePredavacaID);
                        sqlCmd.Parameters.AddWithValue("KorisnikID", predavac.KorisnikID);
                        sqlCmd.Parameters.AddWithValue("DepartmanID", predavac.DepartmanID);
                        sqlCmd.Parameters.AddWithValue("id", predavac.PredavacID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Updating Data");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteKorisnik(int id)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("DELETE FROM Predavac.Predavac where PredavacID=@id"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Deleting Data");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}