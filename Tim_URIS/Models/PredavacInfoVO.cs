﻿namespace Tim_URIS.Models
{
    public class PredavacInfoVO
    {
        public string KatedraPredavaca { get; set; }
        public int ZvanjePredavacaID { get; set; }
        public int KorisnikID { get; set; }
    }
}