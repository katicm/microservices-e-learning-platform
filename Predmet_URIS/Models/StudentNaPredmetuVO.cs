﻿namespace Predmet_URIS.Models
{
    public class StudentNaPredmetuVO
    {
        public string BrojIndeksaStudenta { get; set; }
        public string VrstaStudija { get; set; }
        public string GodinaStudija { get; set; }
        public string KatedraStudenta { get; set; }
    }

}