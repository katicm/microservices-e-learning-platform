﻿using Korisnik_URIS.Exceptions;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;


namespace Korisnik_URIS.DataAccess
{
    public class AccessDataExceptionFilter : ExceptionFilterAttribute
    {
        public override async Task OnExceptionAsync(
            HttpActionExecutedContext actionExecutedContext,
            CancellationToken cancellationToken)
        {
            var ex = actionExecutedContext.Exception;
            if (ex.InnerException is IndexOutOfRangeException)
            {
                actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(ex.Message)
                };
            }
            else if (ex is DataBaseResultException)
            {
                actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(ex.Message)
                };
            }
            await Task.FromResult(0);
        }
    }
}