﻿using Korisnik_URIS.AuthFilters;
using Korisnik_URIS.DataAccess;
using Korisnik_URIS.Filters;
using Korisnik_URIS.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Korisnik_URIS.Controllers
{
    [RoutePrefix("api-korisnik/korisnik")]
    public class KorisnikController : ApiController
    {
        // GET: api-korisnik/korisnik
        [Route("")]
        [HttpGet]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage GetKorisnik()
        {
            var korisnikDB = new KorisnikDB();
            return Request.CreateResponse(HttpStatusCode.OK, korisnikDB.GetAllKorisnik());
        }

        // GET: api-korisnik/korisnik/5
        [HttpGet, Route("{id}")]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        public HttpResponseMessage GetKorisnikByID(int id)
        {
            var korisnikDB = new KorisnikDB();
            return Request.CreateResponse(HttpStatusCode.OK, korisnikDB.GetKorisnikByID(id));
        }

        // POST: api-korisnik/korisnik
        [HttpPost, Route("")]
        [ValidateModelState(BodyRequired = true)]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        public HttpResponseMessage CreateKorisnik([FromBody]Korisnik korisnik)
        {
            var korisnikDB = new KorisnikDB();
            var created = korisnikDB.CreateKorisnik(korisnik);
            var response = Request.CreateResponse(HttpStatusCode.Created, created);
            response.Headers.Location = new Uri(Request.RequestUri + "/" + created.KorisnikID);
            return response;
        }

        // PUT: api-korisnik/korisnik
        [HttpPut, Route("")]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        [ValidateModelState(BodyRequired = true)]
        public HttpResponseMessage UpdateKorisnik([FromBody]Korisnik korisnik)
        {
            var korisnikDB = new KorisnikDB();
            korisnikDB.UpdateKorisnik(korisnik);
            return Request.CreateResponse(HttpStatusCode.OK, "Updated Successfully");
        }

        // DELETE: api-korisnik/korisnik/5
        [HttpDelete, Route("{id}")]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage DeleteKorisnik(int id)
        {
            var korisnikDB = new KorisnikDB();
            korisnikDB.DeleteKorisnik(id);
            return Request.CreateResponse(HttpStatusCode.OK, "Deleted Successfully");
        }
    }
}
