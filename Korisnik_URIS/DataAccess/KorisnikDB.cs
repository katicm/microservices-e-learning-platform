﻿using Korisnik_URIS.Exceptions;
using Korisnik_URIS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Korisnik_URIS.DataAccess
{
    public class KorisnikDB
    {
        public List<Korisnik> GetAllKorisnik()
        {
            try
            {
                List<Korisnik> result = new List<Korisnik>();
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Korisnik.Korisnik", connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (var dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var korisnik = new Korisnik
                            {
                                KorisnikID = Convert.ToInt32(dataReader["KorisnikID"]),
                                Username = Convert.ToString(dataReader["Username"]),
                                Sifra = Convert.ToString(dataReader["Sifra"]),
                                Ime = Convert.ToString(dataReader["Ime"]),
                                Prezime = Convert.ToString(dataReader["Prezime"]),
                                Email = Convert.ToString(dataReader["Email"]),
                                TrenutnaUloga = Convert.ToString(dataReader["TrenutnaUloga"])
                            };
                            result.Add(korisnik);
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new DataBaseResultException("Bad Request - Error Retrieving Data");
            }

        }

        public Korisnik GetKorisnikByID(int id)
        {
            try
            {
                Korisnik korisnik = null;
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Korisnik.Korisnik where KorisnikID=" + id, connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (var dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            korisnik = new Korisnik
                            {
                                KorisnikID = Convert.ToInt32(dataReader["KorisnikID"]),
                                Username = Convert.ToString(dataReader["Username"]),
                                Sifra = Convert.ToString(dataReader["Sifra"]),
                                Ime = Convert.ToString(dataReader["Ime"]),
                                Prezime = Convert.ToString(dataReader["Prezime"]),
                                Email = Convert.ToString(dataReader["Email"]),
                                TrenutnaUloga = Convert.ToString(dataReader["TrenutnaUloga"])
                            };
                        }
                    }
                    if (korisnik == null)
                        throw new DataBaseResultException("Not Found - Error Retrieving Data", new IndexOutOfRangeException());
                }
                return korisnik;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Korisnik CreateKorisnik(Korisnik korisnik)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("INSERT INTO Korisnik.Korisnik VALUES(@Username, @Sifra, @Ime, @Prezime, @Email, @TrenutnaUloga)", connection);
                    sqlCmd.Parameters.AddWithValue("Username", korisnik.Username);
                    sqlCmd.Parameters.AddWithValue("Sifra", korisnik.Sifra);
                    sqlCmd.Parameters.AddWithValue("Ime", korisnik.Ime);
                    sqlCmd.Parameters.AddWithValue("Prezime", korisnik.Prezime);
                    sqlCmd.Parameters.AddWithValue("Email", korisnik.Email);
                    sqlCmd.Parameters.AddWithValue("TrenutnaUloga", korisnik.TrenutnaUloga);

                    connection.Open();
                    sqlCmd.ExecuteNonQuery();
                    SqlCommand sqlKorisnik = new SqlCommand("SELECT TOP 1 * FROM Korisnik.Korisnik ORDER BY KorisnikID DESC", connection);
                    Korisnik last = new Korisnik();
                    using (var korisnikRead = sqlKorisnik.ExecuteReader())
                    {
                        while (korisnikRead.Read())
                        {
                            last.KorisnikID = Convert.ToInt32(korisnikRead["KorisnikID"]);
                            last.Username = Convert.ToString(korisnikRead["Username"]);
                            last.Sifra = Convert.ToString(korisnikRead["Sifra"]);
                            last.Ime = Convert.ToString(korisnikRead["Ime"]);
                            last.Prezime = Convert.ToString(korisnikRead["Prezime"]);
                            last.Email = Convert.ToString(korisnikRead["Email"]);
                            last.TrenutnaUloga = Convert.ToString(korisnikRead["TrenutnaUloga"]);
                        }
                    }
                    return last;
                }
            }
            catch (Exception)
            {
                throw new DataBaseResultException("Unsuccessful Creating Data");
            }

        }

        public void UpdateKorisnik(Korisnik korisnik)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("UPDATE Korisnik.Korisnik SET Username=@Username, Sifra=@Sifra, Ime=@Ime, Prezime=@Prezime, Email=@Email, TrenutnaUloga=@TrenutnaUloga WHERE KorisnikID=@id"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("Username", korisnik.Username);
                        sqlCmd.Parameters.AddWithValue("Sifra", korisnik.Sifra);
                        sqlCmd.Parameters.AddWithValue("Ime", korisnik.Ime);
                        sqlCmd.Parameters.AddWithValue("Prezime", korisnik.Prezime);
                        sqlCmd.Parameters.AddWithValue("Email", korisnik.Email);
                        sqlCmd.Parameters.AddWithValue("TrenutnaUloga", korisnik.TrenutnaUloga);
                        sqlCmd.Parameters.AddWithValue("id", korisnik.KorisnikID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Updating Data");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteKorisnik(int id)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("DELETE FROM Korisnik.Korisnik where KorisnikID=@id"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Deleting Data");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}