﻿using System.Configuration;

namespace Korisnik_URIS.DataAccess
{
    public class DBConnectionString
    {
        public static string ConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString; }
        }
    }
}