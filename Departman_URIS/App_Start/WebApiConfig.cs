﻿using Departman_URIS.AuthFilters;
using Departman_URIS.Filters;
using System.Web.Http;

namespace Departman_URIS
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            config.SuppressHostPrincipal();


            config.Filters.Add(new BasicAuthFilterAttribute());
            config.Filters.Add(new TokenAuthFilter());

            config.Filters.Add(new AuthorizeAttribute());

            config.Filters.Add(new AccessDataExceptionFilter());

            // Web API routes
            config.MapHttpAttributeRoutes();
        }
    }
}
