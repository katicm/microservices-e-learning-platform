﻿using Departman_URIS.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Departman_URIS.DataAccess
{
    public class DepartmanHTTP
    {
        public async Task <List<PredavacVO>>GetPredavacVOAsync(int departmanID,string authorization)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:63295/");
                client.DefaultRequestHeaders.Accept.Clear();
                var token = authorization.Split(null)[1];
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api-predavac/predavac/departman/" + departmanID);
                List<PredavacVO> predavac = new List<PredavacVO>();
                if (response.IsSuccessStatusCode)
                {
                    predavac = await response.Content.ReadAsAsync<List<PredavacVO>>();
                }
                return predavac;
            }
        }
    }
}