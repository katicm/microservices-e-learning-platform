﻿using Departman_URIS.Exceptions;
using Departman_URIS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Departman_URIS.DataAccess
{
    public class FakultetDB
    {
        public List<Fakultet> GetAllFakultet()
        {
            try
            {
                List<Fakultet> result = new List<Fakultet>();
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Departman.Fakultet", connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (SqlDataReader dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var fakultet = new Fakultet
                            {
                                FakultetID = Convert.ToInt32(dataReader["FakultetID"]),
                                NazivFakulteta = Convert.ToString(dataReader["NazivFakulteta"]),
                                UniverzitetID = Convert.ToInt32(dataReader["UniverzitetID"])
                            };
                            result.Add(fakultet);
                        }
                    }
                }
                return result;
            }
            catch(Exception)
            {
                throw new DataBaseResultException("Bad Request - Error Retrieving Data");
            }
        }

        public Fakultet GetFakultetByID(int id)
        {
            try
            {
                Fakultet fakultet = null;
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Departman.Fakultet where FakultetID=" + id, connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (var dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            fakultet = new Fakultet
                            {
                                FakultetID = Convert.ToInt32(dataReader["FakultetID"]),
                                NazivFakulteta = Convert.ToString(dataReader["NazivFakulteta"]),
                                UniverzitetID = Convert.ToInt32(dataReader["UniverzitetID"])
                            };
                        }
                    }
                    if (fakultet == null)
                        throw new DataBaseResultException("Not Found - Error Retrieving Data", new IndexOutOfRangeException());
                }
                return fakultet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Fakultet CreateFakultet(Fakultet fakultet)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("INSERT INTO Departman.Fakultet VALUES(@NazivFakulteta, @UniverzitetID)", connection);
                    sqlCmd.Parameters.AddWithValue("NazivFakulteta", fakultet.NazivFakulteta);
                    sqlCmd.Parameters.AddWithValue("UniverzitetID", fakultet.UniverzitetID);
                    connection.Open();
                    sqlCmd.ExecuteNonQuery();
                    SqlCommand sqlFakultet = new SqlCommand("SELECT TOP 1 * FROM Departman.Fakultet ORDER BY FakultetID DESC", connection);
                    Fakultet last = new Fakultet();
                    using (SqlDataReader fakultetRead = sqlFakultet.ExecuteReader())
                    {
                        while (fakultetRead.Read())
                        {
                            last.FakultetID = Convert.ToInt32(fakultetRead["FakultetID"]);
                            last.NazivFakulteta = Convert.ToString(fakultetRead["NazivFakulteta"]);
                            last.UniverzitetID = Convert.ToInt32(fakultetRead["UniverzitetID"]);
                        }
                    }
                    return last;
                }
            }
            catch (Exception)
            {
                throw new DataBaseResultException("Unsuccessful Creating Data");
            }
        }

        public void UpdateFakultet(Fakultet fakultet)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("UPDATE Departman.Fakultet SET NazivFakulteta=@NazivFakulteta, UniverzitetID=@UniverzitetID WHERE FakultetID=@FakultetID"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("NazivFakulteta", fakultet.NazivFakulteta);
                        sqlCmd.Parameters.AddWithValue("FakultetID", fakultet.FakultetID);
                        sqlCmd.Parameters.AddWithValue("UniverzitetID", fakultet.UniverzitetID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Updating Data");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteFakultet(int id)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("DELETE FROM Departman.Fakultet where FakultetID=@id"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Deleting Data");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}