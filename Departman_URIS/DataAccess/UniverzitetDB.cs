﻿using Departman_URIS.Exceptions;
using Departman_URIS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Departman_URIS.DataAccess
{
    public class UniverzitetDB
    {
        public List<Univerzitet> GetAllUniverzitet()
        {
            try
            {
                List<Univerzitet> result = new List<Univerzitet>();
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Departman.Univerzitet", connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (var dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var univerzitet = new Univerzitet
                            {
                                UniverzitetID = Convert.ToInt32(dataReader["UniverzitetID"]),
                                NazivUniverziteta = Convert.ToString(dataReader["NazivUniverziteta"])
                            };
                            result.Add(univerzitet);
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw new DataBaseResultException("Bad Request - Error Retrieving Data");
            }
        }

        public Univerzitet GetUniverzitetByID(int id)
        {
            try
            {
                Univerzitet univerzitet = null;
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Departman.Univerzitet where UniverzitetID=" + id, connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (var dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            univerzitet = new Univerzitet
                            {
                                UniverzitetID = Convert.ToInt32(dataReader["UniverzitetID"]),
                                NazivUniverziteta = Convert.ToString(dataReader["NazivUniverziteta"])
                            };
                        }
                    }
                    if (univerzitet == null)
                        throw new DataBaseResultException("Not Found - Error Retrieving Data", new IndexOutOfRangeException());
                }
                return univerzitet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Univerzitet CreateUniverzitet(Univerzitet univerzitet)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("INSERT INTO Departman.Univerzitet VALUES(@NazivUniverziteta)", connection);
                    sqlCmd.Parameters.AddWithValue("NazivUniverziteta", univerzitet.NazivUniverziteta);
                    connection.Open();
                    sqlCmd.ExecuteNonQuery();
                    SqlCommand sqlUniverzitet = new SqlCommand("SELECT TOP 1 * FROM Departman.Univerzitet ORDER BY UniverzitetID DESC", connection);
                    Univerzitet last = new Univerzitet();
                    using (var univerzitetRead = sqlUniverzitet.ExecuteReader())
                    {
                        while (univerzitetRead.Read())
                        {
                            last.UniverzitetID = Convert.ToInt32(univerzitetRead["UniverzitetID"]);
                            last.NazivUniverziteta = Convert.ToString(univerzitetRead["NazivUniverziteta"]);
                        }
                    }
                    return last;
                }
            }
            catch (Exception)
            {
                throw new DataBaseResultException("Unsuccessful Creating Data");
            }
        }

        public void UpdateUniverzitet(Univerzitet univerzitet)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("UPDATE Departman.Univerzitet SET NazivUniverziteta=@NazivUniverziteta WHERE UniverzitetID=@id"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("NazivUniverziteta", univerzitet.NazivUniverziteta);
                        sqlCmd.Parameters.AddWithValue("id", univerzitet.UniverzitetID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Updating Data");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteUniverzitet(int id)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("DELETE FROM Departman.Univerzitet where UniverzitetID=@id"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Deleting Data");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}