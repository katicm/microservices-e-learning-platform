﻿using Departman_URIS.Exceptions;
using Departman_URIS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Departman_URIS.DataAccess
{
    public class DepartmanDB
    {
        public List<Departman> GetAllDepartman()
        {
            try
            {
                List<Departman> result = new List<Departman>();
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Departman.Departman", connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (var dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var Departman = new Departman
                            {
                                DepartmanID = Convert.ToInt32(dataReader["DepartmanID"]),
                                NazivDepartmana = Convert.ToString(dataReader["NazivDepartmana"]),
                                FakultetID = Convert.ToInt32(dataReader["FakultetID"])
                            };

                            result.Add(Departman);
                        }
                    }
                }
                return result;
            }
            catch(Exception)
            {
                throw new DataBaseResultException("Bad Request - Error Retrieving Data");
            }
        }

        public DepartmanWithVO GetDepartmanByID(int id)
        {
            try
            {
                DepartmanWithVO departman = null;
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Departman.Departman where DepartmanID=" + id, connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (var dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            departman = new DepartmanWithVO
                            {
                                DepartmanID = Convert.ToInt32(dataReader["DepartmanID"]),
                                NazivDepartmana = Convert.ToString(dataReader["NazivDepartmana"]),
                                FakultetID = Convert.ToInt32(dataReader["FakultetID"])
                            };
                        }
                    }
                    if (departman == null)
                        throw new DataBaseResultException("Not Found - Error Retrieving Data", new IndexOutOfRangeException());
                }
                return departman;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public Departman CreateDepartman(Departman departman)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("INSERT INTO Departman.Departman VALUES(@NazivDepartmana, @FakultetID)", connection);
                    sqlCmd.Parameters.AddWithValue("NazivDepartmana", departman.NazivDepartmana);
                    sqlCmd.Parameters.AddWithValue("FakultetID", departman.FakultetID);
                    connection.Open();
                    sqlCmd.ExecuteNonQuery();
                    SqlCommand sqlDepartman = new SqlCommand("SELECT TOP 1 * FROM Departman.Departman ORDER BY DepartmanID DESC", connection);
                    Departman last = new Departman();
                    using (SqlDataReader DepartmanRead = sqlDepartman.ExecuteReader())
                    {
                        while (DepartmanRead.Read())
                        {
                            last.DepartmanID = Convert.ToInt32(DepartmanRead["DepartmanID"]);
                            last.NazivDepartmana = Convert.ToString(DepartmanRead["NazivDepartmana"]);
                            last.FakultetID = Convert.ToInt32(DepartmanRead["FakultetID"]);
                        }
                    }
                    return last;
                }
            }
            catch(Exception)
            {
                throw new DataBaseResultException("Unsuccessful Creating Data");
            }
        }

        public void UpdateDepartman(Departman departman)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("UPDATE Departman.Departman SET NazivDepartmana=@NazivDepartmana, FakultetID=@FakultetID WHERE Departman.DepartmanID=@DepartmanID"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("NazivDepartmana", departman.NazivDepartmana);
                        sqlCmd.Parameters.AddWithValue("DepartmanID", departman.DepartmanID);
                        sqlCmd.Parameters.AddWithValue("FakultetID", departman.FakultetID);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Updating Data");
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteDepartman(int id)
        {
            try
            {
                using (var connection = new SqlConnection(DBConnectionString.ConnectionString))
                {
                    using (var sqlCmd = new SqlCommand("DELETE FROM Departman.Departman where Departman.DepartmanID=@id"))
                    {
                        connection.Open();
                        sqlCmd.Connection = connection;
                        sqlCmd.Parameters.AddWithValue("id", id);
                        int rowAffected = sqlCmd.ExecuteNonQuery();
                        if (rowAffected == 0)
                            throw new DataBaseResultException("Unsuccessful Deleting Data");
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}