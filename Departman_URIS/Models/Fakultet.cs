﻿namespace Departman_URIS.Models
{
    public class Fakultet
    {
        public int FakultetID { get; set; }
        public string NazivFakulteta { get; set; }
        public int UniverzitetID { get; set; }
    }
}