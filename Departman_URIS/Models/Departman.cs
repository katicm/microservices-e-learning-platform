﻿using System.Collections.Generic;

namespace Departman_URIS.Models
{
    public class Departman
    {
        public int DepartmanID { get; set; }
        public string NazivDepartmana { get; set; }
        public int FakultetID { get; set; }
    }
    public class DepartmanWithVO : Departman
    {
        public List<PredavacVO> Predavac { get; set; }
    }
}