﻿namespace Departman_URIS.Models
{
    public class Univerzitet
    {
        public int UniverzitetID { get; set; }
        public string NazivUniverziteta { get; set; }
    }
}