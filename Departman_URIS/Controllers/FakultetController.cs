﻿using Departman_URIS.AuthFilters;
using Departman_URIS.DataAccess;
using Departman_URIS.Filter;
using Departman_URIS.Filters;
using Departman_URIS.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Departman_URIS.Controllers
{
    [RoutePrefix("api-departman/fakultet")]
    public class FakultetController : ApiController
    {
        // GET: api-departman/fakultet
        [Route("")]
        [HttpGet]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        public HttpResponseMessage GetFakultet()
        {
            var fakultetDB = new FakultetDB();
            return Request.CreateResponse(HttpStatusCode.OK, fakultetDB.GetAllFakultet());
        }

        // GET: api-departman/fakultet/5
        [HttpGet, Route("{id}")]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        public HttpResponseMessage GetFakultetByID(int id)
        {
            var fakultetDB = new FakultetDB();
            return Request.CreateResponse(HttpStatusCode.OK, fakultetDB.GetFakultetByID(id));
        }

        // POST: api-departman/fakultet
        [HttpPost, Route("")]
        [ValidateModelState(BodyRequired = true)]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage CreateFakultet([FromBody]Fakultet fakultet)
        {
            var fakultetDB = new FakultetDB();
            var created = fakultetDB.CreateFakultet(fakultet);
            var response = Request.CreateResponse(HttpStatusCode.Created, created);
            response.Headers.Location = new Uri(Request.RequestUri + "/" + created.FakultetID);
            return response;
        }

        // PUT: api-departman/fakultet
        [HttpPut, Route("")]
        [ValidateModelState(BodyRequired = true)]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage UpdatePredmet([FromBody]Fakultet fakultet)
        {
            var fakultetDB = new FakultetDB();
            fakultetDB.UpdateFakultet(fakultet);
            return Request.CreateResponse(HttpStatusCode.OK, "Updated Successfully");
        }

        // DELETE: api-departman/fakultet/5
        [HttpDelete, Route("{id}")]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage DeleteFakultet(int id)
        {
            var fakultetDB = new FakultetDB();
            fakultetDB.DeleteFakultet(id);
            return Request.CreateResponse(HttpStatusCode.OK, "Deleted Successfully");
        }
    }
}
