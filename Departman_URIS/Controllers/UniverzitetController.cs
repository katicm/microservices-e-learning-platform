﻿using Departman_URIS.AuthFilters;
using Departman_URIS.DataAccess;
using Departman_URIS.Filter;
using Departman_URIS.Filters;
using Departman_URIS.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Departman_URIS.Controllers
{
    [RoutePrefix("api-departman/univerzitet")]
    public class UniverzitetController : ApiController
    {
        // GET: api-departman/univerzitet
        [Route("")]
        [HttpGet]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        public HttpResponseMessage GetUniverzitet()
        {
            var univerzitetDB = new UniverzitetDB();
            return Request.CreateResponse(HttpStatusCode.OK, univerzitetDB.GetAllUniverzitet());
        }

        // GET: api-departman/univerzitet/5
        [HttpGet, Route("{id}")]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        public HttpResponseMessage GetUniverzitetByID(int id)
        {
            var univerzitetDB = new UniverzitetDB();
            return Request.CreateResponse(HttpStatusCode.OK, univerzitetDB.GetUniverzitetByID(id));
        }

        // POST: api-departman/univerzitet
        [HttpPost, Route("")]
        [ValidateModelState(BodyRequired = true)]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage CreateUniverzitet([FromBody]Univerzitet univerzitet)
        {
            var univerzitetDB = new UniverzitetDB();
            var created = univerzitetDB.CreateUniverzitet(univerzitet);
            var response = Request.CreateResponse(HttpStatusCode.Created, created);
            response.Headers.Location = new Uri(Request.RequestUri + "/" + created.UniverzitetID);
            return response;
        }

        // PUT: api-departman/univerzitet
        [HttpPut, Route("")]
        [ValidateModelState(BodyRequired = true)]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage UpdateUniverzitet([FromBody]Univerzitet univerzitet)
        {
            var univerzitetDB = new UniverzitetDB();
            univerzitetDB.UpdateUniverzitet(univerzitet);
            return Request.CreateResponse(HttpStatusCode.OK, "Updated Successfully");
        }

        // DELETE: api-departman/univerzitet/5
        [HttpDelete, Route("{id}")]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage DeleteUniverzitet(int id)
        {
            var univerzitetDB = new UniverzitetDB();
            univerzitetDB.DeleteUniverzitet(id);
            return Request.CreateResponse(HttpStatusCode.OK, "Deleted Successfully");
        }
    }
}
