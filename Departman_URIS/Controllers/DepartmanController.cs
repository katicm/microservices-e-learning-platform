﻿using Departman_URIS.AuthFilters;
using Departman_URIS.DataAccess;
using Departman_URIS.Filter;
using Departman_URIS.Filters;
using Departman_URIS.Models;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Departman_URIS.Controllers
{
    [RoutePrefix("api-departman/departman")]
    public class DepartmanController : ApiController
    {
        // GET: api-departman/departman
        [Route("")]
        [HttpGet]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        public HttpResponseMessage GetDepartman()
        {
            var departmanDB = new DepartmanDB();
            return Request.CreateResponse(HttpStatusCode.OK, departmanDB.GetAllDepartman());
        }

        // GET: api-departman/departman/5
        [HttpGet, Route("{id}")]
        [Authorize(Roles = RolesConst.ROLE_Admin_Student_Predavac)]
        [ClientCacheControlFilter(ClientCacheControl.Private, 5)]
        public async Task<HttpResponseMessage> GetDepartmanByIDAsync(int id)
        {
            var departmanDB = new DepartmanDB();
            DepartmanWithVO departman = departmanDB.GetDepartmanByID(id);
            var departmanHttp = new DepartmanHTTP();
            var authorization = Request.Headers.GetValues("Authorization").FirstOrDefault();
            departman.Predavac = await departmanHttp.GetPredavacVOAsync(departman.DepartmanID, authorization);
            return Request.CreateResponse(HttpStatusCode.OK, departman);
        }

        // POST: api-departamn/departman
        [HttpPost, Route("")]
        [ValidateModelState(BodyRequired = true)]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage CreateDepartman([FromBody]Departman departman)
        {
            var departmanDB = new DepartmanDB();
            var created = departmanDB.CreateDepartman(departman);
            var response = Request.CreateResponse(HttpStatusCode.Created, created);
            response.Headers.Location = new Uri(Request.RequestUri + "/" + created.DepartmanID);
            return response;
        }

        // PUT: api-departman/departman
        [HttpPut, Route("")]
        [ValidateModelState(BodyRequired = true)]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage UpdateDepartman([FromBody]Departman departman)
        {
            var departmanDB = new DepartmanDB();
            departmanDB.UpdateDepartman(departman);
            return Request.CreateResponse(HttpStatusCode.OK, "Updated Successfully");
        }

        // DELETE: api-departman/departman
        [HttpDelete, Route("{id}")]
        [Authorize(Roles = RolesConst.ROLE_Admin)]
        public HttpResponseMessage DeleteDepartman(int id)
        {
            var departmanDB = new DepartmanDB();
            departmanDB.DeleteDepartman(id);
            return Request.CreateResponse(HttpStatusCode.OK, "Deleted Successfully");
        }
    }
}
