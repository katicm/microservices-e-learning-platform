﻿namespace Kurs_URIS.Models
{
    public class PredavacKursaVO
    {
        public string KatedraPredavaca { get; set; }
        public int ZvanjePredavacaID { get; set; }
        public int KorisnikID { get; set; }
    }
}