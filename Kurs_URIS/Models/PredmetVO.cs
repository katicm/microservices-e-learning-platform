﻿namespace Kurs_URIS.Models
{
    public class PredmetVO
    {
        public string NazivPredmeta { get; set; }
        public int ECTSBodovi { get; set; }
    }
}