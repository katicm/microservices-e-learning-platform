﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Student_URIS_Core.DataAccess;
using Student_URIS_Core.Models;

namespace Student_URIS_Core.Controllers
{
    [Route("api-student/student")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        public readonly StudentDB studentDB;
        public StudentController(IConfiguration Configuration)
        {
            studentDB = new StudentDB(Configuration["ConnectionString:DBCS"]);
        }

        [HttpGet("")]
        public ActionResult<List<Student>> GetStudent()
        {
            return Ok(studentDB.GetAllStudent());
        }

        [HttpGet("{id}")]
        public ActionResult<Student>GetStudentByID(int id)
        {
            return Ok(studentDB.GetStudentByID(id));
        }
    }
}