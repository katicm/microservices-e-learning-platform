﻿using Microsoft.Extensions.Configuration;
using Student_URIS_Core.Exceptions;
using Student_URIS_Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Student_URIS_Core.DataAccess
{
    public class StudentDB
    { 
        public readonly string ConnectionString;
        public StudentDB(string ConnectionString)
        {
            this.ConnectionString = ConnectionString;
        }
        public List<Student> GetAllStudent()
        {
            try
            {
                List<Student> result = new List<Student>();
                using (var connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Student.Student", connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (SqlDataReader dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            var student = new Student
                            {
                                StudentID = Convert.ToInt32(dataReader["StudentID"]),
                                DatumRodjenjaStudenta = Convert.ToDateTime(dataReader["DatumRodjenjaStudenta"]),
                                MestoRodjenjaStudenta = Convert.ToString(dataReader["MestoRodjenjaStudenta"]),
                                BrojIndeksaStudenta = Convert.ToString(dataReader["BrojIndeksaStudenta"]),
                                KatedraStudenta = Convert.ToString(dataReader["KatedraStudenta"]),
                                StudijskiProgramStudenta = Convert.ToString(dataReader["StudijskiProgramStudenta"]),
                                VrstaStudija = Convert.ToString(dataReader["VrstaStudija"]),
                                StepenStudija = Convert.ToString(dataReader["StepenStudija"]),
                                GodinaStudija = Convert.ToString(dataReader["GodinaStudija"]),
                                RbrUpisaneGodine = Convert.ToInt32(dataReader["RbrUpisaneGodine"]),
                                GodinaUpisaFakulteta = Convert.ToInt32(dataReader["GodinaUpisaFakulteta"]),
                                NacinFinansiranja = Convert.ToString(dataReader["NacinFinansiranja"]),
                                SifraStudenta = Convert.ToString(dataReader["SifraStudenta"]),
                                KorisnikID = Convert.ToInt32(dataReader["KorisnikID"]),
                                DepartmanID = Convert.ToInt32(dataReader["DepartmanID"])
                            };

                            result.Add(student);
                        }
                    }
                }
                return result;
            }
            catch(Exception)
            {
                throw new DataBaseResultException("Bad Request - Error Retrieving Data");
            }
        }
        public Student GetStudentByID(int id)
        {
            try
            {
                StudentWithVO student = null;
                using (var connection = new SqlConnection(ConnectionString))
                {
                    SqlCommand sqlCmd = new SqlCommand("select * from Student.Student where StudentID=" + id, connection)
                    {
                        CommandType = CommandType.Text
                    };
                    connection.Open();
                    using (SqlDataReader dataReader = sqlCmd.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            student = new StudentWithVO
                            {
                                StudentID = Convert.ToInt32(dataReader["StudentID"]),
                                DatumRodjenjaStudenta = Convert.ToDateTime(dataReader["DatumRodjenjaStudenta"]),
                                MestoRodjenjaStudenta = Convert.ToString(dataReader["MestoRodjenjaStudenta"]),
                                BrojIndeksaStudenta = Convert.ToString(dataReader["BrojIndeksaStudenta"]),
                                KatedraStudenta = Convert.ToString(dataReader["KatedraStudenta"]),
                                StudijskiProgramStudenta = Convert.ToString(dataReader["StudijskiProgramStudenta"]),
                                VrstaStudija = Convert.ToString(dataReader["VrstaStudija"]),
                                StepenStudija = Convert.ToString(dataReader["StepenStudija"]),
                                GodinaStudija = Convert.ToString(dataReader["GodinaStudija"]),
                                RbrUpisaneGodine = Convert.ToInt32(dataReader["RbrUpisaneGodine"]),
                                GodinaUpisaFakulteta = Convert.ToInt32(dataReader["GodinaUpisaFakulteta"]),
                                NacinFinansiranja = Convert.ToString(dataReader["NacinFinansiranja"]),
                                SifraStudenta = Convert.ToString(dataReader["SifraStudenta"]),
                                KorisnikID = Convert.ToInt32(dataReader["KorisnikID"]),
                                DepartmanID = Convert.ToInt32(dataReader["DepartmanID"])
                            };
                        }
                    }
                    if (student == null)
                        throw new DataBaseResultException("Not Found - Error Retrieving Data", new IndexOutOfRangeException());
                }
                return student;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
