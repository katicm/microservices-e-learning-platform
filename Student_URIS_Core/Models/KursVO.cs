﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Student_URIS_Core.Models
{
    public class KursVO
    {
        public string NazivKursa { get; set; }
        public string RasporedPredavanja { get; set; }
        public string StudijskiProgram { get; set; }
        public string SkolskaGodina { get; set; }
    }
}
