﻿using System.ComponentModel.DataAnnotations;

namespace Student_URIS_Core.Models
{
    public class Test
    {
        public int TestID { get; set; }

        [Required]
        public int KursID { get; set; }

        [Required]
        [MaxLength(100)]
        public string NazivTesta { get; set; }
    }

    public class TestWithVO : Test
    {
        public KursVO Kurs { get; set; }
    }
}
