﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Student_URIS_Core.Models
{
    public class StudentTest
    {
        public int StudentTestID { get; set; }

        [Required]
        public int TestID { get; set; }

        [Required]
        public int StudentID { get; set; }
    }
}
