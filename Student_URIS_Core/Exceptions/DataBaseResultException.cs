﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Student_URIS_Core.Exceptions
{
    public class DataBaseResultException : Exception
    {
        public DataBaseResultException()
        {

        }

        public DataBaseResultException(string message) : base(message)
        {

        }

        public DataBaseResultException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
